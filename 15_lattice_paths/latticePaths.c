/* latticePaths
 *
 * Find the maximum amount of ways that can be traveled in a square 20x20 grid
 * The constraints are that only movements down and to the right are allowed
 * 
 * Author: Jeremy Tobac
 *
 * Date: October 2014
 */
#include <stdio.h>
#include <stdlib.h>

#define ROWS 20
#define COLUMNS 20
#define PRINT_ARRAY 0

/**@brief Initialize an array of intLength to zero
 *
 *@param int long pintlAray: Array to be set to zero
 *@param in intLength: Length of the array
 */
void init_array(int long *pintlArray, int intLength)
{
	int intI = 0;

	for(intI=0;intI<intLength;intI++){
		pintlArray[intI]=0;
	}
}

/**@brief Fill in array with amount of paths that can be taken from each point
 *
 * Fills in each cell of a square array with the number of ways that can be moved from the current cell
 * to the bottom right cell if the only moves allowed are to the right and down
 *
 *@param int long *pintlArray: 20x20 array representing square grid to be traveled
 *@param int intRows: Number of rows in the grid
 *@param int intColumns: Number of columns in the grid 
 */
void fill_array(int long *pintlArray, int intRows, int intColumns)
{
	int intX, intY, intTempY;
	int long intlTemp;

	for(intX=(intColumns - 1); intX >= 0; intX--){
		for(intY=(intRows - 1); intY >= 0; intY--){
			intlTemp = 0;
			if(intX == (intColumns - 1)){
				pintlArray[intY * intRows + intX] = 1;
			}
			else{
				for(intTempY = intY;intTempY < intRows;intTempY++){
					intlTemp += pintlArray[intTempY * intRows + intX + 1];
				}
				pintlArray[intY * intRows + intX] = intlTemp;
			}
		}
	}
}

/**@brief Print out all of the cells in an array
 *
 *@param int long *pintlArray: 20x20 array representing square grid to be traveled
 *@param int intRows: Number of rows in the grid
 *@param int intColumns: Number of columns in the grid 
 */
void print_array(int long *pintlArray, int intRows, int intColumns)
{
	int intX, intY;

	for(intY = 0; intY < intRows; intY++){
		for(intX = 0; intX < intColumns; intX++){
			printf("%012ld ", pintlArray[intY * intRows + intX]);
		}
		printf("\n");
	}
}

/**@brief Find the total number of paths that can be travelled in the lattice paths
 *
 * Find the total number of paths that can be travelled in the lattice paths
 * This is done by adding up all of the numbers in each cell representing the number of paths 
 * that can be taken from that cell
 * Add one to the final result to get the true total, because the outside path is not counted
 * inside the individual paths
 *
 *@param int long *pintlArray: 20x20 array representing square grid to be traveled
 *@param int intRows: Number of rows in the grid
 *@param int intColumns: Number of columns in the grid 
 *
 *@ret Returns the total number of paths that can be traveled in the 20x20 array
 */
int long find_result(int long*pintlArray, int intRows, int intColumns)
{
	int long intlResult = 0;
	int intI;
	for(intI = 0; intI < (intRows * intColumns); intI++){
		intlResult += pintlArray[intI];
	}

	return (intlResult + 1);
}

int main()
{
	int long *pintlArray, intlResult;
	pintlArray = (int long*)malloc(sizeof(int long)*ROWS*COLUMNS);
	if(pintlArray == NULL){
		fprintf(stderr, "Could not malloc space for array\n");
		return -1;
	}

	init_array(pintlArray, (int)ROWS * (int)COLUMNS);
	
	fill_array(pintlArray, (int)ROWS, (int)COLUMNS);

#if PRINT_ARRAY
	print_array(pintlArray, (int)ROWS, (int)COLUMNS);
#endif
	intlResult = find_result(pintlArray, (int)ROWS, (int)COLUMNS);

	printf("\nTotal: %ld\n", intlResult);

	free(pintlArray);
	pintlArray = NULL;
	if(pintlArray){
		fprintf(stderr, "error freeing memory\n");
		return -1;
	}

	return 0;
}
